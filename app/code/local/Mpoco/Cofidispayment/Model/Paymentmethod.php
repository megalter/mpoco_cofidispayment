<?php
class Mpoco_Cofidispayment_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract{
	protected $_code  = 'cofidispayment';
	protected $_formBlockType = 'cofidispayment/form_cofidispayment';
	protected $_infoBlockType = 'cofidispayment/info_cofidispayment';

	public function assignData($data){
//		$info = $this->getInfoInstance();
//
//		if ($data->getCustomFieldOne())
//			$info->setCustomFieldOne($data->getCustomFieldOne());
//		
//
//		if ($data->getCustomFieldTwo())
//			$info->setCustomFieldTwo($data->getCustomFieldTwo());
		
		return $this;
	}

	/**
	 * Validates custom inputs (in case of need) for Codifis type payment
	 * 
	 * @return \Mpoco_Cofidispayment_Model_Paymentmethod
	 */
	public function validate(){
		parent::validate();
//		$info = $this->getInfoInstance();

//		if (!$info->getCustomFieldOne()){
//		  $errorCode = 'invalid_data';
//		  $errorMsg = $this->_getHelper()->__("CustomFieldOne is a required field.\n");
//		}

//		if (!$info->getCustomFieldTwo()){
//			$errorCode = 'invalid_data';
//			$errorMsg .= $this->_getHelper()->__('CustomFieldTwo is a required field.');
//		}

//		if ($errorMsg) 
//			Mage::throwException($errorMsg);

		return $this;
	}

	public function getOrderPlaceRedirectUrl(){
		return Mage::getUrl('cofidispayment/payment/redirect', array('_secure' => false));
	}
}