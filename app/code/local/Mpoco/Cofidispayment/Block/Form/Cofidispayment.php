<?php
class Mpoco_Cofidispayment_Block_Form_Cofidispayment extends Mage_Payment_Block_Form
{
  protected function _construct()
  {
    parent::_construct();
    $this->setTemplate('cofidispayment/form/cofidispayment.phtml');
  }
}