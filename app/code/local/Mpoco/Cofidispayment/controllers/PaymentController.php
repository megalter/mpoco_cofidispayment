<?php
/*
Title: Cofidis Payment Controller
Author: Miguel Poço 
*/
	
class Mpoco_Cofidispayment_PaymentController extends Mage_Core_Controller_Front_Action{
	  
	public function redirectAction(){
		$this->loadLayout();
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template','cofidispayment',array('template' => 'cofidispayment/redirect.phtml'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
	}

	public function responseAction(){
		$orderId = $this->getRequest()->get("orderId");
		$totalPaid = $this->getRequest()->get("totalPaid");
		$token = $this->getRequest()->get("token");
		$flag = $this->getRequest()->get("flag");
		if ($flag == "1" && $orderId && $this->decodeToken($token, $totalPaid, $orderId)){
			error_log('stuff');
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
			$order->setTotalPaid($totalPaid);
			$order->setState(Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW, true, 'Cofidis - Pagamento com sucesso: ' . $totalPaid . '€.');
			$order->save();

			Mage::getSingleton('checkout/session')->unsQuoteId();
			Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/success', array('_secure'=> false));
		}
		else{
			Mage_Core_Controller_Varien_Action::_redirect('checkout/onepage/error', array('_secure'=> false));
		}
	}
	
	private function decodeToken($token, $total, $id){
		return hash('sha256', $total . $id . 'haSHk3y') == $token;
	}
}