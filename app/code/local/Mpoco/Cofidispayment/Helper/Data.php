<?php
class Mpoco_Cofidispayment_Helper_Data extends Mage_Core_Helper_Abstract{
	function getPaymentGatewayUrl(){
		return Mage::getUrl('cofidispayment/payment/gateway', array('_secure' => false));
	}
}